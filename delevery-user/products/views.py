from rest_framework.viewsets import ModelViewSet

from products.models import Product, ProductCategory
from products.serializers import ProductSerializer, ProductCategorySerializer


class ProductView(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductCategoryView(ModelViewSet):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer
