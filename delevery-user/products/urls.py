from django.urls import path

from products.views import ProductView, ProductCategoryView

urlpatterns = [
    path('', ProductView.as_view({'post': 'create', 'get': 'list'})),
    path('<int:pk>', ProductView.as_view({'post': 'update', 'get': 'retrieve', 'delete': 'destroy'})),
    path('category', ProductCategoryView.as_view({'post': 'create', 'get': 'list'})),
    path('category/<int:pk>', ProductCategoryView.as_view({'post': 'update', 'get': 'retrieve', 'delete': 'destroy'})),
]