from django.contrib import admin

from user.models import Profiles, Stuff, Courier

admin.site.register(Profiles)
admin.site.register(Stuff)
admin.site.register(Courier)
