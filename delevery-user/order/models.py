from django.db import models


class OrderStatuses(models.IntegerChoices):
    NEW = 1, 'New'
    APPROVED = 2, 'Approved'
    ON_ROAD = 3, 'On road'
    FINISHED = 4, 'Finished'
    CANCELED = 5, 'Canceled'


class Order(models.Model):
    client = models.ForeignKey('user.Profiles', models.SET_NULL, 'profile_orders', null=True)
    courier = models.ForeignKey('user.Courier', models.SET_NULL, 'courier_orders', null=True)
    address = models.CharField('Адрес', max_length=255)
    total_price = models.PositiveIntegerField('Цена', null=True)
    status = models.IntegerField('Статус', choices=OrderStatuses.choices, default=1)
    date = models.DateTimeField('Дата и время заказа', auto_now_add=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return f'{self.address}'


class OrderProducts(models.Model):
    order = models.ForeignKey(Order, models.CASCADE, 'order_products')
    product = models.ForeignKey('products.Product', models.SET_NULL, 'product_order', null=True)
    count = models.PositiveSmallIntegerField('Количество')

    class Meta:
        verbose_name = 'Товар заказа'
        verbose_name_plural = 'Товары заказа'

    def __str__(self):
        return f'{self.product.name}'
