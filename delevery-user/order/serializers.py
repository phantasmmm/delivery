from rest_framework import serializers

from order.models import Order, OrderProducts


class OrderProductSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = OrderProducts
        fields = ('id', 'product', 'count')


class OrderCreateSerializer(serializers.ModelSerializer):
    order_products = OrderProductSerializer(many=True)

    class Meta:
        model = Order
        fields = ('address', 'order_products')

    # def create(self, validated_data):
    #     user = self.context.get('request').user.profile
    #     order = Order.objects.create(client=user, **validated_data)
    #     return order
