from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from order.models import Order, OrderProducts
from order.serializers import OrderCreateSerializer
from products.models import Product
from .serializers import OrderCreateSerializer


class OrderViewNoSer(ModelViewSet):
    serializer_class = OrderCreateSerializer

    def create(self, request, *args, **kwargs):
        user = request.user.profile
        address_from_data = request.data.get('address')
        order_products = request.data.get('order_products')
        print('-'*80)
        print(order_products)

        order_created = Order.objects.create(client=user, address=address_from_data)

        total_sum = 0

        for product in order_products:
            price = Product.objects.get(id=product.get('product_id')).price
            total_sum += price * product.get('count')

            OrderProducts.objects.create(
                order=order_created,
                product_id=product.get('product_id'),
                count=product.get('count')
            )

        order_created.total_price = total_sum
        order_created.save()

        return Response('Success')


class OrderView(ModelViewSet):
    serializer_class = OrderCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

